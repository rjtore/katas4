const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

// Write a helper function that can be called within each kata function.

/*
let header = document.createElement("div");
header.textContent = "Kata 1";
document.body.appendChild(header);
*/

katas = [
    function (stringToSplit, splitBy, sepBy) {
        let splitString = stringToSplit.split(splitBy);
        let jsonifiedList = JSON.stringify(splitString); //can I use for kata 4?

        if (sepBy == ";") {
            jsonifiedList = jsonifiedList.replace(/,/g, sepBy)
        }
        return jsonifiedList;
    },
    /*function(arrayToSplit)
    {
        let joinedString = arrayToSplit;
        return joinedString;
    }*/
]

function kata1() {
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return gotCitiesCSV; // Don't forget to return your output!
}

function main() {
    for (let i = 0; i < 32; i++) {
        let result;

        switch (i + 1) {
            case 1: {
                result = katas[0](gotCitiesCSV, ",", ",");
                break;
            }
            case 2: {
                result = katas[0](bestThing, " ", ",");
                break;
            }
            case 3: {
                result = katas[0](gotCitiesCSV, ",", ";");
                break;
                // OLGA -- WOULDN'T BE ABLE TO EXPLAIN
            }
            case 4: {
                //result = kata(lotrCitiesArray, ",", ","); //split nor join work
                result = lotrCitiesArray.join(',');
                break;
            }
            case 5: {
                result = lotrCitiesArray.slice(0, 5);
                break;
            }
            case 6: {
                result = lotrCitiesArray.slice(3, 8);
                break;
            }
            case 7: {
                result = lotrCitiesArray.slice(2, 5);
                break;
            }
            case 8: {
                lotrCitiesArray.splice(2, 1);
                result = lotrCitiesArray;
                // result = lotrCitiesArray.splice(lotrCitiesArray.indexOf("Rohan"), 1);
                break;
                // 8.	Write a function that uses 'splice' to remove 'Rohan' from 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. 
                // NEED TO RESET THE ARRAY TO SHOW REMAINS OF ARRAY
            }
            case 9: {
                lotrCitiesArray.splice(5, 2);
                //lotrCitiesArray.splice(5, 7 - 5);

                //result = lotrCitiesArray.splice(5, lotrCitiesArray.length - 5);
                result = lotrCitiesArray;

                break;
                // 9.	Write a function that uses 'splice' to remove all cities after 'Dead Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 10: {
                lotrCitiesArray.splice(2, 0, "Rohan");
                result = lotrCitiesArray;
                break;
                // 10.	Write a function that uses 'splice' to add 'Rohan' back to 'lotrCitiesArray' right after 'Gondor' and returns the new modified 'lotrCitiesArray'. 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 11: {
                lotrCitiesArray.splice(5, 1, "Deadest Marshes");
                result = lotrCitiesArray;
                break;
                // 11.	Write a function that uses 'splice' to rename 'Dead Marshes' to 'Deadest Marshes' in 'lotrCitiesArray' and returns the new modified 'lotrCitiesArray'. 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 12: {
                result = bestThing.slice(0, 15);
                break;
                // 12.	Write a function that uses 'slice' to return a string with the first 14 characters from 'bestThing'. 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 13: {
                result = bestThing.slice(bestThing.length - 13);
                break;
                // 13.	Write a function that uses 'slice' to return a string with the last 12 characters from 'bestThing'. 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 14: {
                result = bestThing.slice(23, 38);
                break;
                // 14.	Write a function that uses 'slice' to return a string with the characters between the 23rd and 38th position of 'bestThing' (i.e., 'boolean is even'). 
                // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
            }
            case 15: {
                result = bestThing.substring(bestThing.length - 13);
                break;
                // 15.	Write a function that does the exact same thing as #13 but use the 'substring' method instead of 'slice'. 
            }
            case 16: {
                result = bestThing.substring(23, 38);
                break;
                // 16.	Write a function that does the exact same thing as #14 but use the 'substring' method instead of 'slice'. 
            }

            case 17: {
                result = bestThing.indexOf("only");
                break;
                // const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
                // 17.	Write a function that finds and returns the index of 'only' in 'bestThing'. 
            }

            case 18: {
                result = bestThing.indexOf("bit");
                break;
                // CHECK WORK w/ED !!!
                // 18.	Write a function that finds and returns the index of the last word in 'bestThing'. 
            }

            case 19: {
                let array = gotCitiesCSV.split(",");
                let doubleVowelCities = [];
                let doubleVowelList = ["aa", "ee"];
                for (city in array) {
                    for (let doubleVowelListCounter = 0; doubleVowelListCounter < doubleVowelList.length; doubleVowelListCounter++) {
                        if (array[city].includes(doubleVowelList[doubleVowelListCounter])) {
                            doubleVowelCities.push(array[city]);
                            break;
                        }
                    }
                }

                result = JSON.stringify(doubleVowelCities);
                // string.includes("aa,", "ee")
                // result = gotCitiesCSV
                // console.log(array);
                // console.log(array.includes);
                break;

                // 19.	Write a function that FINDS AND RETURNS an array of all cities from 'gotCitiesCSV' that use double vowels ('aa', 'ee', etc.). 
                // split creates array
                // "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
            }

            case 20: {
                result = [];
                for (let lotrCitiesArrayCounter = 0; lotrCitiesArrayCounter < lotrCitiesArray.length; lotrCitiesArrayCounter++) {
                    if (lotrCitiesArray[lotrCitiesArrayCounter].endsWith("or")) {
                        result.push(lotrCitiesArray[lotrCitiesArrayCounter]);
                    }
                }
                result = JSON.stringify(result);
                break;
                // 20.	Write a function that FINDS AND RETURNS an array with all cities from 'lotrCitiesArray' that end with 'or'. 
                // // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"]
            }

            case 21: {
                let bestThingArray = bestThing.split(" ");
                result = [];
                for (let i = 0; i < bestThingArray.length; i++) {
                    if (bestThingArray[i].startsWith("b")) {
                            result.push(bestThingArray[i]);
                    }
                }
                result = JSON.stringify(result);
                break;

                // 21.	Write a function that FINDS AND RETURNS an array with all the words in 'bestThing' that start with a 'b'. 
                // "The BEST thing about a BOOLEAN is even if you are wrong you are only off BY a BIT"
            }

            case 22: {
                // 22.	Write a function that RETURNS 'Yes' or 'No' IF 'lotrCitiesArray' includes 'Mirkwood'. 
                // if  
                if(lotrCitiesArray.includes("Mirkwood")) result = "Yes";
                else result = "No";
                break;

            }

            case 23: {
                // 23.	Write a function that RETURNS 'Yes' or 'No' IF 'lotrCitiesArray' includes 'Hollywood'. 
                if(lotrCitiesArray.includes("Hollywood")) result = "Yes";
                else result = "No";
                break;;

            }

            case 24: {
                result = lotrCitiesArray.indexOf("Mirkwood");
                break;
                // 24.	Write a function that RETURNS THE INDEX OF 'Mirkwood' in 'lotrCitiesArray'. 
            }

            case 25: {
                for (let i = 0; i < lotrCitiesArray.length; i++) {
                if (lotrCitiesArray[i].includes(" ")) {
                    result = lotrCitiesArray[i];
                    break;
                }
            }
                // 25.	Write a function that FINDS AND RETURNS the first city in 'lotrCitiesArray' that has more than one word. 
                break;
            }
            // ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

            case 26: {
                result = JSON.stringify(lotrCitiesArray.reverse());
                //result = result.join();
                //result = katas[0](result, ",", ",");
                // 26.	Write a function that REVERSES THE ORDER of 'lotrCitiesArray' and returns the new array. 
                break;
            }

            case 27: {
                result = JSON.stringify(lotrCitiesArray.sort());
                // 27.	Write a function that SORTS 'lotrCitiesArray' alphabetically and returns the new array. 
                break;
            }

            case 28: {
                result = JSON.stringify(lotrCitiesArray.sort());
                let temp;
                let myArray = []
                for(let i = 0; i < lotrCitiesArray.length; i++)
                {
                    temp = lotrCitiesArray[i];
                    for(let j = 0; j < lotrCitiesArray.length; j++)
                    {
                        if(temp.length > lotrCitiesArray[i].length) {
                            temp = lotrCitiesArray[i];
                        }
                    }
                    myArray.push(temp);
                }  
                result = JSON.stringify(myArray);
                // 28.	Write a function that SORTS 'lotrCitiesArray' by the number of characters in each city (i.e., shortest city names go first) and return the new array. 
                break;
            }

            case 29: {
                lotrCitiesArray.pop();
                result = JSON.stringify(lotrCitiesArray);
                // 29.	Write a function that USES "POP" to remove the last city from 'lotrCitiesArray' and returns the new array. 
                break;
            }

            case 30: {
                lotrCitiesArray.push("Rohan");
                result = JSON.stringify(lotrCitiesArray);

                // 30.	Write a function that USES "PUSH" to add back the city from 'lotrCitiesArray' that was removed in #29 to the back of the array and returns the new array. 
                break;
            }

            case 31: {
                lotrCitiesArray.shift();
                result = JSON.stringify(lotrCitiesArray);

                // 31.	Write a function that USES "SHIFT" to remove the first city from 'lotrCitiesArray' and returns the new array. 
                break;
            }

            case 32: {
                lotrCitiesArray.unshift("Beleriand");
                result = JSON.stringify(lotrCitiesArray);
                // 32.	Write a function that USES "UNSHIFT" to add back the city from 'lotrCitiesArray' that was removed in #31 to the front of the array and returns the new array. 
                break;
            }

            // case 19: {
            //     doubleVowels = ["aa", "ee", "ii", "oo", "uu"];
            //     newArray = []

            //     for(let i = 0; i < gotCitiesCSV.length; i++)
            //      {
            //         for(let j = 0; j < doubleVowels.length; j++)
            //         {
            //             if(gotCitiesCSV[i].indexOf(doubleVowels[j]) !== -1)
            //             {
            //                 newArray.push(gotCitiesCSV[i]);
            //                 break;
            //             }
            //         }
            //     }
            //     result = newArray;
            //     break;
            // }
            // 19.	Write a function that finds and returns an array of all cities from 'gotCitiesCSV' that use double vowels ('aa', 'ee', etc.). 
            //["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

        }

        let paragraph = document.createElement("p");
        let textNode = document.createTextNode("Kata " + (i + 1) + "\n" + result);
        paragraph.appendChild(textNode);
        document.body.appendChild(paragraph);
    }
}

main();

// 24.	Write a function that returns the index of 'Mirkwood' in 'lotrCitiesArray'. 
// 25.	Write a function that finds and returns the first city in 'lotrCitiesArray' that has more than one word. 
// 26.	Write a function that reverses the order of 'lotrCitiesArray' and returns the new array. 
// 27.	Write a function that sorts 'lotrCitiesArray' alphabetically and returns the new array. 
// 28.	Write a function that sorts 'lotrCitiesArray' by the number of characters in each city (i.e., shortest city names go first) and return the new array. 
// 29.	Write a function that uses 'pop' to remove the last city from 'lotrCitiesArray' and returns the new array. 
// 30.	Write a function that uses 'push' to add back the city from 'lotrCitiesArray' that was removed in #29 to the back of the array and returns the new array. 
// 31.	Write a function that uses 'shift' to remove the first city from 'lotrCitiesArray' and returns the new array. 
// 32.	Write a function that uses 'unshift' to add back the city from 'lotrCitiesArray' that was removed in #31 to the front of the array and returns the new array. 
